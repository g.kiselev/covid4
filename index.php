<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Result to save';
  }

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  // TODO: аналогично все поля.
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['gender'] = !empty($_COOKIE['gender_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['super'] = !empty($_COOKIE['super_error']);
  $errors['bio'] = !empty($_COOKIE['bio_error']);
  $errors['check'] = !empty($_COOKIE['check_error']);
  
  // Выдаем сообщения об ошибках.
  if ($errors['fio']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('fio_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">FIll NAME.</div>';
  }
  if ($errors['email']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('email_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">FIll mail.</div>';
  }
  
  if ($errors['date']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('date_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">FIll Date.</div>';
  }
  
  if ($errors['gender']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('date_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">select to  gender.</div>';
  }
  if ($errors['limbs']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('limbs_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Select to limbs.</div>';
  }
  
  if ($errors['super']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('super_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Select to superpowers.</div>';
  }
  
  
  if ($errors['bio']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('bio_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">fill bio.</div>';
  }
  
  
  if ($errors['check']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('check_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">check.</div>';
  }
  

  // TODO: тут выдать сообщения об ошибках в других полях.

  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['date'] = empty($_COOKIE['date_value']) ? '' : $_COOKIE['date_value'];
  $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
  $values['super'] = empty($_COOKIE['super_value']) ? '' : $_COOKIE['super_value'];
  $values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
  $values['check'] = empty($_COOKIE['check_value']) ? '' : $_COOKIE['check_value'];
  // TODO: аналогично все поля.

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['fio'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['email'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('email_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на месяц.
      setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['date'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('date_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на месяц.
      setcookie('date_value', $_POST['date'], time() + 30 * 24 * 60 * 60);
  }

  
  if (empty($_POST['radio-group-1'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('gender_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на месяц.
      setcookie('gender_value', $_POST['radio-group-1'], time() + 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['radio-group-2'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('limbs_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на месяц.
      setcookie('limbs_value', $_POST['radio-group-2'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['field-name-2'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('super_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на месяц.
      setcookie('super_value', $_POST['field-name-2'], time() + 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['bio'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('bio_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на месяц.
      setcookie('bio_value', $_POST['bio'], time() + 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['check-1'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('check_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на месяц.
      setcookie('check_value', $_POST['check-1'], time() + 30 * 24 * 60 * 60);
  }
// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('date_error', '', 100000);
    setcookie('gender_error', '', 100000);
    setcookie('limbs_error', '', 100000);
    setcookie('super_error', '', 100000);
    setcookie('bio_error', '', 100000);
    setcookie('check_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
  }

  // Сохранение в XML-документ.
  // ...

  $user = 'u16654';
  $pass = '8728536';
  $db = new PDO('mysql:host=localhost;dbname=u16654', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  
  // Подготовленный запрос. Не именованные метки.
  try {
      $stmt = $db->prepare("INSERT INTO application SET name = ?, mail = ?, dat = ?, gender = ?, limbs = ?, superpowers = ?, biography = ?");
      //  $stmt -> execute(array('fio'));
      // $stmt = $db->prepare("INSERT INTO application SET name = ?, mail = ?");
      
      $stmt->execute(array($_POST['fio'], $_POST['email'],$_POST['date'],$_POST['radio-group-1'],$_POST['radio-group-2'],
          $_POST['field-name-2'],$_POST['bio']));
      
      // $stmt->execute(array($_POST['fio'], $_POST['mail']));
      
      
      
  }
  catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
  }
  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
}
